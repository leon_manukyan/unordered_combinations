class Vertex:
    pass


class TopVertex(Vertex):
    def __init__(self, i):
        self.group=[i]
        self.adj_bottom_vertices = set()

    def group_index(self):
        return self.group[0]


class BottomVertex(Vertex):
    def __init__(self, i):
        self.i = i
        self.adj_top_vertices = set()

    def is_hard_matched(self):
        return len(self.adj_top_vertices) == 1

    def can_be_eliminated(self):
        return len(self.adj_top_vertices) == 2


class BipartiteGraph:
    def __init__(self, top_vertex_count):
        self.top_vertex2group_map = [i for i in range(top_vertex_count)]
        self.top_vertices = {i:TopVertex(i) for i in range(top_vertex_count)}
        self.bottom_vertices = {}
        self.bottom_vertex_counter = 0

    def todict(self):
        d = {}
        for i,t in self.top_vertices.items():
            d[i] = t.group, [bv.i for bv in t.adj_bottom_vertices]
        return d

    def add_bottom_vertex(self, adj_top_vertices):
        new_bottom_vertex = self.just_add_bottom_vertex(adj_top_vertices)
        return self.simplify_at(new_bottom_vertex)


    def just_add_bottom_vertex(self, adj_top_vertices):
        new_bottom_vertex = BottomVertex(self.bottom_vertex_counter)
        for t in adj_top_vertices:
            top_vertex = self.top_vertices[self.top_vertex2group_map[t]]
            top_vertex.adj_bottom_vertices.add(new_bottom_vertex)
            new_bottom_vertex.adj_top_vertices.add(top_vertex)
        self.bottom_vertices[new_bottom_vertex.i] = new_bottom_vertex
        self.bottom_vertex_counter += 1
        return new_bottom_vertex


    def simplify_at(self, bottom_vertex):
        bottom_vertex_queue = [bottom_vertex]
        killed_top_vertices = set()
        while len(bottom_vertex_queue) > 0:
            bottom_vertex = bottom_vertex_queue.pop()
            result = self.simplify_once_at(bottom_vertex)
            killed_top_vertices |= set(result[0])
            bottom_vertex_queue.extend(result[1])
        return list(killed_top_vertices)


    def simplify_once_at(self, bottom_vertex):
        if bottom_vertex.is_hard_matched():
            return self.hard_match_bottom_vertex(bottom_vertex)
        elif bottom_vertex.can_be_eliminated():
            return [], self.eliminate_bottom_vertex(bottom_vertex)
        else:
            return [], []


    def hard_match_bottom_vertex(self, v):
        assert v.is_hard_matched()
        self.remove_bottom_vertex(v)
        return self.kill_top_vertex(v.adj_top_vertices.pop())


    def eliminate_bottom_vertex(self, bv):
        assert bv.can_be_eliminated()
        self.remove_bottom_vertex(bv)
        merged_vertex = self.merge_top_vertices(*bv.adj_top_vertices)
        return merged_vertex.adj_bottom_vertices


    def remove_bottom_vertex(self, bv):
        for tv in bv.adj_top_vertices:
            tv.adj_bottom_vertices.remove(bv)
        del self.bottom_vertices[bv.i]


    def kill_top_vertex(self, v):
        del self.top_vertices[v.group_index()]
        for bv in v.adj_bottom_vertices:
            bv.adj_top_vertices.remove(v)
        return v.group, v.adj_bottom_vertices


    def merge_top_vertices(self, v1, v2):
        if v1.group_index() > v2.group_index():
            v1, v2 = v2, v1

        for bv in v2.adj_bottom_vertices:
            bv.adj_top_vertices.add(v1)
            v1.adj_bottom_vertices.add(bv)

        self.merge_top_vertex_groups(v1, v2)
        self.remove_top_vertex(v2)
        return v1


    def merge_top_vertex_groups(self, v1, v2):
        for v in v2.group:
            self.top_vertex2group_map[v] = v1.group_index()
        v1.group.extend(v2.group)


    def remove_top_vertex(self, tv):
        for bv in tv.adj_bottom_vertices:
            bv.adj_top_vertices.remove(tv)
        del self.top_vertices[tv.group_index()]


if __name__ == '__main__':
    import unittest

    class Tests(unittest.TestCase):

        def setUp(self):
            pass

        def tearDown(self):
            pass

        def test_flow1(self):
            g = BipartiteGraph(5)
            self.assertEqual(g.todict(),
                    {
                        0: ([0], []),
                        1: ([1], []),
                        2: ([2], []),
                        3: ([3], []),
                        4: ([4], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([0])
            self.assertEqual(killed_top_vertices, [0])
            self.assertEqual(g.todict(),
                    {
                        1: ([1], []),
                        2: ([2], []),
                        3: ([3], []),
                        4: ([4], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([1,2])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        1: ([1,2], []),
                        3: ([3], []),
                        4: ([4], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([3,4])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        1: ([1,2], []),
                        3: ([3,4], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([2,4])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        1: ([1,2,3,4], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([3])
            self.assertEqual(killed_top_vertices, [1, 2, 3, 4])
            self.assertEqual(g.todict(),
                    {
                    }
            )


        def test_flow2(self):
            g = BipartiteGraph(3)
            self.assertEqual(g.todict(),
                    {
                        0: ([0], []),
                        1: ([1], []),
                        2: ([2], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([0,1,2])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        0: ([0], [0]),
                        1: ([1], [0]),
                        2: ([2], [0]),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([0,1])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        0: ([0, 1, 2], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([0,2])
            self.assertEqual(killed_top_vertices, [0,1,2])
            self.assertEqual(g.todict(),
                    {
                    }
            )

        def test_flow3(self):
            g = BipartiteGraph(3)
            self.assertEqual(g.todict(),
                    {
                        0: ([0], []),
                        1: ([1], []),
                        2: ([2], []),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([0,1,2])
            self.assertEqual(killed_top_vertices, [])
            self.assertEqual(g.todict(),
                    {
                        0: ([0], [0]),
                        1: ([1], [0]),
                        2: ([2], [0]),
                    }
            )

            killed_top_vertices = g.add_bottom_vertex([1])
            self.assertEqual(killed_top_vertices, [1])
            self.assertEqual(g.todict(),
                    {
                        0: ([0,2], []),
                    }
            )

    unittest.main()
