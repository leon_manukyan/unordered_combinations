from matching import BipartiteGraph

def make_graph(pools, all_values):
    g = BipartiteGraph(len(pools))

    for v in all_values:
        g.add_bottom_vertex(v, 0, [i for i,p in enumerate(pools) if v in p])

    return g

class GenUnorderedCombinations:
    def __init__(self, pools):
        self.pools = pools
        self.all_values = set()
        for p in pools:
            self.all_values.update(p)
        self.g = make_graph(self.pools, self.all_values)

    def is_feasible(self, prefix):
        self.g.reset_bottom_vertex_degrees()
        for x in prefix:
            self.g.increment_bottom_vertex_degree(x)
        return self.g.is_matchable()

    def generate_combinations(self, prefix):
        if not self.is_feasible(prefix):
            return
        elif len(prefix) == len(self.pools):
            yield prefix
        else:
            for v in self.all_values:
                if v >= prefix[-1]:
                    yield from self.generate_combinations(prefix + (v,))

    def __iter__(self):
        for v in self.all_values:
            yield from self.generate_combinations((v,))


def unordered_combinations(pools):
    yield from GenUnorderedCombinations(pools)

if __name__ == '__main__':
    import unordered_combinations_simple
    simple_impl = unordered_combinations_simple.unordered_combinations

    import sys
    if len(sys.argv) == 2 and sys.argv[1] == '--self-test':
        del sys.argv[1]
        import unittest
        class Tests(unittest.TestCase):

            def setUp(self):
                pass

            def tearDown(self):
                pass

            def check(self, pools):
                result=list(unordered_combinations(pools))
                self.assertEqual(simple_impl(pools), result)

            def test(self):
                self.check([[1, 2, 3]])
                self.check([[1, 2, 3], [4, 5], [6, 7]])
                self.check([[1, 2, 3], [3, 4], [4, 5]])
                self.check([[1, 2, 3], [2, 3, 4, 5], [3, 4, 6]])
                self.check([[1, 2, 3], [1, 2, 3], [1, 2, 3]])
                self.check([[1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5]])

        unittest.main()
    else:
        from time import time
        import os, psutil
        import argparse
        parser = argparse.ArgumentParser(description='')
        parser.add_argument('--check-correctness', action="store_true")
        parser.add_argument('pools')
        parser.add_argument('display_condition', default='True')
        args = parser.parse_args()

        pools = eval(args.pools)
        pools = [sorted(p) for p in pools]
        must_be_displayed=eval('lambda i:'+args.display_condition)

        print('Pools:')
        for p in pools:
            print(' ', p)

        if args.check_correctness:
            del sys.argv[1]

            start_time = time()
            correct_combos = simple_impl(pools)
            elapsed_time = time() - start_time

            print('\nComputed correct result in {:.2f}s'.format(elapsed_time))

            def check_combination(i, c):
                if c != correct_combos[i]:
                    print('Incorrect combination #{}!!!'.format(i))
                    print('Actual:  {}'.format(c))
                    print('Correct: {}'.format(correct_combos[i]))
                    sys.exit(1)
        else:
            def check_combination(i, c):
                pass


        thisproc = psutil.Process(os.getpid())

        last_displayed = None
        def display(i, c):
            global last_displayed
            last_displayed = i
            elapsed_time = time() - start_time
            mem_usage = thisproc.memory_info().rss//1024//1024
            print('{}: {} ({:.1f}s {}MB)'.format(i, c, elapsed_time, mem_usage))

        start_time = time()
        print('\nCombinations:')
        for i,c in enumerate(unordered_combinations(pools)):
            check_combination(i, c)
            if must_be_displayed(i):
                display(i, c)

        if i != last_displayed:
            display(i, c)
