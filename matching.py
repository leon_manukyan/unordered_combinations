from collections import deque

class Vertex:
    def __init__(self, id):
        self.id = id
        self.edges = []


class TopVertex(Vertex):
    degree = 1
    pass

class BottomVertex(Vertex):
    def __init__(self, id, degree=1):
        super().__init__(id)
        self.degree = degree

class Edge:
    def __init__(self, top_vertex, bottom_vertex):
        self.top_vertex = top_vertex
        self.bottom_vertex = bottom_vertex
        top_vertex.edges.append(self)
        bottom_vertex.edges.append(self)

class BipartiteGraph:
    def __init__(self, top_vertex_count):
        self.top_vertices = {i:TopVertex(i) for i in range(top_vertex_count)}
        self.bottom_vertices = {}

    def add_bottom_vertex(self, key, degree, adj_top_vertices):
        new_bottom_vertex = BottomVertex(key, degree)
        for t in adj_top_vertices:
            top_vertex = self.top_vertices[t]
            new_edge = Edge(top_vertex, new_bottom_vertex)
        self.bottom_vertices[key] = new_bottom_vertex

    def reset_bottom_vertex_degrees(self):
        for bv in self.bottom_vertices.values():
            bv.degree = 0

    def increment_bottom_vertex_degree(self, key):
        self.bottom_vertices[key].degree += 1

    def is_matchable(self):
        return self.match() is not None

    def match(self):
        matching = set()
        for bv in self.bottom_vertices.values():
            for _ in range(bv.degree):
                aug_path = self.get_augmenting_path(matching, bv)
                if aug_path is None:
                    return None
                self.augment_matching(matching, aug_path)

        return matching

    class VertexVisitCounter:
        def __init__(self, g):
            self.g = g
            self.remaining_visits = {}

        def visit(self, v):
            if v not in self.remaining_visits:
                self.remaining_visits[v] = v.degree
            r = self.remaining_visits[v]
            if r == 0:
                return False
            self.remaining_visits[v] = r - 1
            return True

    def get_augmenting_path(self, matching, bottom_vertex):
        used_top_vertices = set(e.top_vertex for e in matching)
        vertex_visit_counter = BipartiteGraph.VertexVisitCounter(self)
        q = deque()
        q.append((bottom_vertex, None))
        while len(q) > 0:
            v, path = q.popleft()
            if type(v) is BottomVertex:
                for e in filter(lambda e: e not in matching, v.edges):
                    next_v = e.top_vertex
                    if next_v not in used_top_vertices:
                        return (e, path)
                    if vertex_visit_counter.visit(next_v):
                        q.append((next_v, (e,path)))
            else:
                for e in filter(lambda e: e in matching, v.edges):
                    next_v = e.bottom_vertex
                    if vertex_visit_counter.visit(next_v):
                        q.append((next_v, (e,path)))
        return None


    def augment_matching(self, matching, path):
        e, path = path
        matching.add(e)
        while path is not None:
            e, path = path
            matching.remove(e)
            assert path is not None
            e, path = path
            matching.add(e)

