def unordered_combinations(pools):
    combos = {()}
    for pool in pools:
        combos = {tuple(sorted(c + (elem,))) for c in combos for elem in pool}
    return sorted(combos)

if __name__ == '__main__':
    from sys import argv
    pools = eval(argv[1])
    for c in unordered_combinations(pools):
        print(c)
